<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Detail ${listDetail.title}</title>
</head>
<body>
<h3>Id: ${listDetail.id} Title: ${listDetail.title}</h3>
<table border="1">
		<caption>Tasks</caption>
		<tr>
			<th>Name</th>
		</tr>
		<c:forEach var="task" items="${listDetail.tasks}">
			<tr>
				<td>${task.title}</td>
				<c:forEach var="operation" items="${operations.entrySet()}">
					<td><a href="${pageContext.request.contextPath}/task/${operation.key}?id=${task.id}">${operation.value}</a></td>
				</c:forEach>
			</tr>
		</c:forEach>
		
</table>
<br>
<a href="${pageContext.request.contextPath}/">Return to all lists</a>
</body>
</html>