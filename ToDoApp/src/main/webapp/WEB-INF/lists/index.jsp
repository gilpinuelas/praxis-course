<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>To Do List</title>
</head>
<body>
	<h3>ToDo</h3>
	<table border="1">
		<caption>Tasks List</caption>
		<tr>
			<th>Task</th>
			<th colspan="3">Actions</th>
		</tr>
		<c:forEach var="list" items="${lists}">
			<tr>
				<td>${list.title}</td>				
				<c:forEach var="operation" items="${operations.entrySet()}">
					<td><a href="${pageContext.request.contextPath}/lists/${operation.key}?id=${list.id}">${operation.value}</a></td>
				</c:forEach>
			</tr>
		</c:forEach>
	</table>
	<br />
	<br />
	<form action="${pageContext.request.contextPath}/lists/create"
		method="post">
		<fieldset>
			<legend>Create Task</legend>
			Task Title: <input type="text" name="title"	placeholder="Task Title" />
			<input type="submit" value="Create" />
		</fieldset>
	</form>
</body>
</html>