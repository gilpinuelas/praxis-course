<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Update Task: ${toUpdate.title}</title>
</head>
<body>
	<form action="" method="post">
		<fieldset>
			<legend>Update Task: ${toUpdate.title}</legend>
			New Title: <input type="text" name="title" value="${toUpdate.title}" placeholder="Update Task Title" />
			<br /> 
			<input type="submit" value="Update" />
		</fieldset>
	</form>
</body>
</html>