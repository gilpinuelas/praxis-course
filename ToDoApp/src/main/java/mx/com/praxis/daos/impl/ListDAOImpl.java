package mx.com.praxis.daos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import mx.com.praxis.daos.ListDAO;
import mx.com.praxis.entities.TasksList;

@Stateless
public class ListDAOImpl implements ListDAO {

	@PersistenceContext(name = "AccessDataBase")
	private EntityManager entityManager;

	@Override
	public List<TasksList> findAll() {
		TypedQuery<TasksList> queryToFindAllList = entityManager.createNamedQuery("List.findAll", TasksList.class);
		return queryToFindAllList.getResultList();
	}

	@Override
	public TasksList findById(Long id) {
		TypedQuery<TasksList> queryToFindById = entityManager.createNamedQuery("List.findById", TasksList.class);
		queryToFindById.setParameter("id", id);
		return queryToFindById.getSingleResult();
	}

	@Override
	public void create(TasksList toSave) {
		entityManager.persist(toSave);

	}

	@Override
	public void update(TasksList toUpdate) {
		entityManager.merge(toUpdate);
	}

	@Override
	public void delete(TasksList toDelete) {
		entityManager.remove(toDelete);

	}

	/*
	 * public void delete(Long id) { delete(findById(id)); }
	 */

}
