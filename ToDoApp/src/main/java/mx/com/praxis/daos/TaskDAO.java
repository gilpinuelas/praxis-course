package mx.com.praxis.daos;

import java.util.List;

import javax.ejb.Local;

import mx.com.praxis.entities.Task;
import mx.com.praxis.entities.TasksList;

@Local
public interface TaskDAO extends GenericDAO<Task, Long> {

	List<Task> findTaskByIdList(Long id);
	
	List<Long> findIdsByIdList(Long id);
	
	void deleteTaskByIdList(Long id);
}
