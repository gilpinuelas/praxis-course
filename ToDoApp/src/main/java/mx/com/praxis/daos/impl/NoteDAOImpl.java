package mx.com.praxis.daos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.praxis.daos.NoteDAO;
import mx.com.praxis.entities.Note;

@Stateless
public class NoteDAOImpl implements NoteDAO {

	@PersistenceContext(name = "AccessDataBase")
	private EntityManager entityManager;

	@Override
	public List<Note> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Note findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void create(Note toSave) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Note toUpdate) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Note toDelete) {
		entityManager.remove(toDelete);

	}

	@Override
	public List<Note> findNoteByIdTask(Long id) {
		TypedQuery<Note> queryToFindNoteByIdTask = entityManager.createNamedQuery("Note.findAllNotesByIdTask",
				Note.class);
		queryToFindNoteByIdTask.setParameter("idTask", id);
		return queryToFindNoteByIdTask.getResultList();
	}

	@Override
	public void deleteByIdTask(Long idTask) {
		Query queryToDeleteNoteByIdTask = entityManager.createNamedQuery("Note.deleteNoteByIdTask");
		queryToDeleteNoteByIdTask.setParameter("idTask", idTask);
		queryToDeleteNoteByIdTask.executeUpdate();
	}

}
