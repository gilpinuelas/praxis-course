package mx.com.praxis.daos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.praxis.daos.TaskDAO;
import mx.com.praxis.entities.Task;

@Stateless
public class TaskDAOImpl implements TaskDAO {

	@PersistenceContext(name = "AccessDataBase")
	private EntityManager entityManager;

	@Override
	public java.util.List<Task> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Task findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void create(Task toSave) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Task toUpdate) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Task toDelete) {
		entityManager.remove(toDelete);

	}

	@Override
	public List<Task> findTaskByIdList(Long id) {
		TypedQuery<Task> queryToFindAllTasksByIdList = entityManager.createNamedQuery("Task.findAllTasksByIdList",
				Task.class);
		queryToFindAllTasksByIdList.setParameter("idList", id);
		return queryToFindAllTasksByIdList.getResultList();
	}

	@Override
	public List<Long> findIdsByIdList(Long idList) {
		TypedQuery<Long> queryToFindIdsByIdList = entityManager.createNamedQuery("Task.findIdsByIdList", Long.class);
		queryToFindIdsByIdList.setParameter("idList", idList);
		return queryToFindIdsByIdList.getResultList();
	}

	@Override
	public void deleteTaskByIdList(Long idList) {
		Query queryToDeleteByIdList = entityManager.createNamedQuery("Task.deleteTaskByIdList");
		queryToDeleteByIdList.setParameter("idList", idList);
		queryToDeleteByIdList.executeUpdate();
	}

}
