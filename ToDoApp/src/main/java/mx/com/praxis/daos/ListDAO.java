package mx.com.praxis.daos;

import javax.ejb.Local;

import mx.com.praxis.entities.TasksList;

// ejbs -> declarar interfaz (remota, local)
@Local
public interface ListDAO extends GenericDAO<TasksList, Long> {
	
}
