package mx.com.praxis.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import mx.com.praxis.daos.ListDAO;
import mx.com.praxis.daos.NoteDAO;
import mx.com.praxis.daos.TaskDAO;
import mx.com.praxis.dtos.ListDetailDto;
import mx.com.praxis.dtos.ListDto;
import mx.com.praxis.service.ListService;
import mx.com.praxis.utils.Converter;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
public class ListServiceImpl implements ListService {

	@Inject
	private ListDAO listDao;

	@Inject
	private TaskDAO taskDao;

	@Inject
	private NoteDAO noteDao;

	@Override
	public List<ListDto> getAllListDto() {
		return Converter.convertListsToListDto(listDao.findAll());
	}

	@Override
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void create(ListDto toCreate) {
		listDao.create(Converter.converterListDtoToList(toCreate));
	}

	@Override
	public ListDto getListDtoById(Long id) {
		return Converter.convertListToListDto(listDao.findById(id));
	}

	@Override
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void update(ListDto toUpdate) {
		listDao.update(Converter.convertListDtoToListUpdate(toUpdate));
	}

	@Override
	public void delete(Long id) {
		taskDao.findIdsByIdList(id).forEach(noteDao::deleteByIdTask);
		taskDao.deleteTaskByIdList(id);
		listDao.delete(listDao.findById(id));
	}

	@Override
	public ListDetailDto getListDetailDtoById(Long id) {
		return Converter.convertListToListDetailDto(listDao.findById(id), taskDao.findTaskByIdList(id));
	}

}
