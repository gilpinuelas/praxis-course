package mx.com.praxis.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.praxis.dtos.ListDto;
import mx.com.praxis.service.ListService;
import mx.com.praxis.utils.Converter;

@WebServlet(urlPatterns = { "/lists/create", "/lists/read", "/lists/update", "/lists/delete" })
public class ListController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private ListService listService;
	
	private static final Map<String, String> OPS = new HashMap<>();

	static {
		OPS.put("read", "Read");
		OPS.put("update", "Update");
		OPS.put("delete", "Delete");
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operation = request.getServletPath();

		if ("/lists/read".equals(operation)) {
			readList(request, response);
		}
		if ("/lists/update".equals(operation)) {
			updateListGet(request, response);
		}
		if ("/lists/delete".equals(operation)) {
			deleteList(request, response);
		}
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operation = request.getServletPath();

		if ("/lists/create".equals(operation)) {
			createList(request, response);
		}
		if ("/lists/update".equals(operation)) {
			updateList(request, response);
		}
	}

	private void readList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Long id = Long.parseLong(request.getParameter("id"));
		request.setAttribute("operations", OPS);
		request.setAttribute("listDetail", listService.getListDetailDtoById(id));
		request.getRequestDispatcher("/WEB-INF/lists/detail.jsp").forward(request, response);
	}

	private void createList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ListDto toCreate = Converter.converterRequestToListDto(request);
		listService.create(toCreate);

		String url = request.getContextPath() + "/";
		response.sendRedirect(url);
	}

	private void updateList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ListDto toUpdate = Converter.convertRequestToListDtoUpdate(request);
		listService.update(toUpdate);

		String url = request.getContextPath() + "/";
		response.sendRedirect(url);
	}

	private void updateListGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Long id = Long.parseLong(request.getParameter("id"));
		request.setAttribute("toUpdate", listService.getListDtoById(id));
		request.getRequestDispatcher("/WEB-INF/lists/update.jsp").forward(request, response);

	}

	private void deleteList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Long id = Long.parseLong(request.getParameter("id"));
		listService.delete(id);

		String url = request.getContextPath() + "/";
		response.sendRedirect(url);
	}
}
