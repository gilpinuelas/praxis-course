package com.finaltest.thread;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

public class ThreadTest {

	private static final Logger logger = LogManager.getLogger();

	public void createThreads() {
		String userName = "userName";
		String myName = "Gilberto Piñuelas";
		String tcName = "name";

		Runnable log1 = () -> {
			ThreadContext.put(tcName, myName);
			ThreadContext.push(userName, "Super Mario 64");
			logger.log(Level.ALL, "ALL LEVEL");
		};

		Thread thread1 = new Thread(log1);

		Runnable log2 = () -> {
			ThreadContext.put(tcName, myName);
			ThreadContext.push(userName, "Golden Eye 007");
			logger.fatal("FATAL level");
		};

		Thread thread2 = new Thread(log2);

		Runnable log3 = () -> {
			ThreadContext.put(tcName, myName);
			ThreadContext.push(userName, "The Legend Of Zelda: Ocarina of Time");
			logger.error("ERROR level", new NullPointerException("NullError"));
		};

		Thread thread3 = new Thread(log3);

		Runnable log4 = () -> {
			ThreadContext.put(tcName, myName);
			ThreadContext.push(userName, "Mario Kart 64");
			logger.warn("WARN level");
		};

		Thread thread4 = new Thread(log4);

		Runnable log5 = () -> {
			ThreadContext.put(tcName, myName);
			ThreadContext.push(userName, "Banjo-Kazooie");
			logger.info("INFO level");
		};

		Thread thread5 = new Thread(log5);

		Runnable log6 = () -> {
			ThreadContext.put(tcName, myName);
			ThreadContext.push(userName, "Perferct Dark");
			logger.debug("DEBUG level");
		};

		Thread thread6 = new Thread(log6);

		Runnable log7 = () -> {
			ThreadContext.put(tcName, myName);
			ThreadContext.push(userName, "Start Fox 64");
			logger.trace("TRACE level");
		};

		Thread thread7 = new Thread(log7);

		Runnable log8 = () -> {
			ThreadContext.put(tcName, myName);
			ThreadContext.push(userName, "Donkey Kong 64");
			logger.log(Level.getLevel("MUSTFIX"), "MUSTFIX LEVEL");
		};

		Thread thread8 = new Thread(log8);

		Runnable log9 = () -> {
			ThreadContext.put(tcName, myName);
			ThreadContext.push(userName, "Pókemon Stadium");
			logger.log(Level.getLevel("DATABASE"), "DATABASE LEVEL");
		};

		Thread thread9 = new Thread(log9);

		Runnable log10 = () -> {
			ThreadContext.put(tcName, myName);
			ThreadContext.push(userName, "Conker's Bad Fur Day");
			logger.log(Level.getLevel("FAILOVER"), "FAILOVER LEVEL");
		};

		Thread thread10 = new Thread(log10);

		Runnable log11 = () -> {
			ThreadContext.put(tcName, myName);
			ThreadContext.push(userName, "Pókemon Snap");
			logger.log(Level.getLevel("FIXLATER"), "FIXLATER LEVEL");
		};

		Thread thread11 = new Thread(log11);

		Runnable log12 = () -> {
			ThreadContext.put(tcName, myName);
			ThreadContext.push(userName, "Diddy Kong Racing");
			logger.log(Level.getLevel("MYDEBUG"), "MYDEBUG LEVEL");
		};

		Thread thread12 = new Thread(log12);

		Runnable log13 = () -> {
			ThreadContext.put(tcName, myName);
			ThreadContext.push(userName, "Diddy Kong Racing");
			logger.log(Level.OFF, "OFF LEVEL");
		};

		Thread thread13 = new Thread(log13);

		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();
		thread6.start();
		thread7.start();
		thread8.start();
		thread9.start();
		thread10.start();
		thread11.start();
		thread12.start();
		thread13.start();
	}
}
