package com.finaltest.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp2.BasicDataSource;

public class JdbcConfig {

	private static BasicDataSource dataSource;

	private JdbcConfig() {

	}

	public static Connection getConnection() throws SQLException {
		if (dataSource == null) {
			dataSource = new BasicDataSource();
			dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
			dataSource.setUrl("jdbc:oracle:thin:@192.168.56.102:1521:ORCL");
			dataSource.setUsername("ENLACE");
			dataSource.setPassword("enlace");
		}
		return dataSource.getConnection();
	}
}
