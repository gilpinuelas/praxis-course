SECCIÓN 1

1.- Es el lenguage que permite definir estructuras de datos en las que se almacenarán los mismos.

2.- Con este nivel de aislamiento se podrían leer los datos aun no comprometidos.

3.- Se refiere a que una base de datos no puede tener valores de claves externas sin su par.

4.- Para acceder o encontrar de una manera más rápida a datos.

5.- BEGIN - Para indicar que se trata de una transacción.
    COMMIT - Para comprometer una operación y reflejarse en BD.
    ROLLBACK - Para cancelar la transacción y deshacer cambio en los datos.
    SAVEPOINT - ES un punto intermedio de guardado en una transacción a la que se podría volver si ocurre algún error.

6.- Con UNION se combinarán los resultados de las consultas indicadas en un sólo conjunto de datos, 
    con JOIN se crea una cruza de datos entre las tablas y campos indicados.
    
7.- Para definir condiciones en las consultas y definir flujos.

-- ejemplo

SELECT nombre, 
       CASE 
         WHEN fec_retiro IS NULL THEN 'ACTIVO' 
         ELSE 'BAJA' || fec_retiro
       END 
FROM   profesor 
WHERE  Extract(year FROM fec_ingreso) < 2009; 

SECCIÓN 2

1. - CREATE TABLE alumno (
    id_alumno integer NOT NULL,
    nombre varchar(50) NOT NULL,
    num_cuenta varchar(20) NOT NULL,
    licenciatura varchar(15),
    CONSTRAINT id_alumno PRIMARY KEY (id_alumno),
    CONSTRAINT num_cuenta UNIQUE (num_cuenta)
);

2.- CREATE TABLE profesor (
    id_profesor integer,
    nombre varchar(50) NOT NULL,
    num_empleado varchar(10) NOT NULL,
    fec_ingreso date NOT NULL,
    fec_retiro date,
    CONSTRAINT id_profesor PRIMARY KEY (id_profesor),
    CONSTRAINT fec_ingreso CHECK (fec_ingreso < current_date),
    CONSTRAINT fec_retiro CHECK (fec_retiro < current_date)
);

3.- CREATE TABLE materia (
    id_materia integer NOT NULL,
    nombre_materia varchar(30) NOT NULL,
    calificacion numeric(4, 2),
    id_alumno integer,
    id_profesor integer,
    CONSTRAINT id_materia PRIMARY KEY (id_materia),
    CONSTRAINT id_alumno FOREIGN KEY (id_alumno)
        REFERENCES alumno (id_alumno),
    CONSTRAINT calificacion CHECK (calificacion >= 0 AND calificacion <= 10)
);

4.- CREATE TABLE calificacion (
    id_materia integer NOT NULL,
    id_alumno integer,
    id_profesor integer,
    calificacion numeric(4, 2),
    CONSTRAINT "id_materia,id_alumno" PRIMARY KEY (id_materia, id_alumno),
    CONSTRAINT id_materia FOREIGN KEY (id_materia)
        REFERENCES materia (id_materia),
    CONSTRAINT id_alumno FOREIGN KEY (id_materia)
        REFERENCES alumno (id_alumno),
    CONSTRAINT id_profesor FOREIGN KEY (id_profesor)
        REFERENCES profesor (id_profesor),
    CONSTRAINT calificacion CHECK (calificacion >= 0 AND calificacion <= 10)
);

-- 5.- Obtenga el nombre y el promedio de cada alumno.

    SELECT a.nombre, AVG(c.calificacion)
        FROM alumno a JOIN calificacion c
            ON a.id_alumno = c.id_alumno
        GROUP BY a.nomre, c.calificacion
        ORDER BY a.nombre;

-- 6.- Obtenga la materia que más ha sido reprobada

SELECT m.nombre_materia MATERIA, 
FROM   materia m 
       JOIN calificacion c 
         ON m.id_materia = c.id_materia 
WHERE  c.calificacion < 7 
GROUP BY m.nombre_materia 
ORDER BY m.nombre_materia; 

-- 7.- Obtenga el profesor que más personas ha reprobado (calificación mínima aprobatoria es 7).

SELECT p.nombre 
FROM   profesor p 
       JOIN calificacion c 
         ON p.id_profesor = c.id_profesor 
WHERE  c.calificacion < 7 
GROUP BY p.nombre; 

-- 8.- ¿Existe algún alumno que no tenga una calificación asociada?

SELECT * FROM alumno a
LEFT OUTER JOIN calificacion c 
ON a.id_alumno = c.id_alumno
WHERE c Is NULL;

-- 9.- Obtenga la lista de profesores que ya no están activos

SELECT * FROM profesor
    where fec_retiro is not null;

-- 10.- ¿Existe alguna materia que jamás se haya dado?

SELECT * FROM MATERIA WHERE ID_MATERIA IN (
 SELECT ID_MATERIA FROM MATERIA
	 WHERE ID_PROFESOR IS NULL
	 AND ID_ALUMNO IS NULL
 );

-- 11.- Obtenga el listado de profesores cuya fecha de ingreso fue anterior al año 2009

SELECT * FROM profesor
    WHERE extract(year from fec_ingreso) < 2009;