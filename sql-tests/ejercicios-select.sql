-- 1. ¿Cuántas órdenes hay por nación y región?

SELECT n.name, r.name, count(*) "#_ORDENES"
  FROM ORDERS o 
    JOIN CUSTOMER c ON c.custkey = o.custkey
    JOIN NATION n ON n.nationkey = c.nationkey
    JOIN REGION r ON r.regionkey = n.regionkey
  GROUP BY r.name, n.name;

-- 2. ¿Todos los suppliers tienen un partsupplier?

SELECT sup.name, count(psup.suppkey)
  FROM SUPPLIER sup
    LEFT JOIN PARTSUPP psup
    	ON sup.suppkey = psup.suppkey
  GROUP BY sup.name;
  
-- 3. ¿Cuántas órdenes hay por día de orden (orderdate)

SELECT orderdate, COUNT(orderdate) 
  FROM ORDERS
 GROUP BY orderdate;

-- 4. ¿Cuál es el promedio de impuestos (tax) pagados por año?  

SELECT EXTRACT(YEAR, FROM(o.orderdate)) anio, AVG(li.tax) promedio
  FROM ORDERS o 
    JOIN LINEITEM li 
    	ON li.orderkey = o.orderkey
  GROUP BY EXTRACT(YEAR, FROM(o.orderdate));

-- 5. ¿Cuál es el tamaño que más se repite en todos los comentarios?

SELECT LENGTH(comment), COUNT(*) TAMANIO 
   FROM LINEITEM
 GROUP BY LENGTH(comment)
 HAVING COUNT(comment) > 1;

-- 6. ¿Cuál fue el orderpriority más utilizado?

SELECT orderpriority, COUNT(orderpriority)
   FROM ORDERS
 GROUP BY orderpriority
 HAVING MAX(COUNT(orderpriority)) = COUNT(orderpriority);

-- 7. ¿Existe una relación de la nación con el orderpriority?
/*
Se puede decir que sí, por la relación que CUSTOMER tiene con ORDERS, 
suponiendo que las prioridades se asignan con ponderación, de acuerdo a la nación de envío.
*/
SELECT n.name nacion, o.orderprioriry prioridad
  FROM NATION n
    JOIN CUSTOMER c
      ON n.nationkey = c.nationkey
    JOIN ORDERS o
      ON c.custkey = o.custkey
  GROUP BY n.name, o.orderpriority;