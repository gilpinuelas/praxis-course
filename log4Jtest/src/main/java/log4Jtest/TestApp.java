package log4Jtest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestApp {

	private static final Logger logger = LogManager.getLogger();

	public static void main(String[] args) {
		logger.trace("Test Trace");
		logger.debug("Test Debug");
		logger.info("Test Inof");
		logger.warn("Test Warn");
		logger.error("Test Error");
		logger.fatal("Test Fatal");
	}
}
